using System;
using System.Numerics;
using FastExpLib;

namespace SimpleNumberLib
{
    public class lib
    {
        private static Random rand = new Random();

        private static byte getByte(bool lastByte)
        {
            byte newByte = 0;
            for (int index = 0; index < 8; index++)
            {
                newByte |= (byte)rand.Next(2);
                newByte = (byte)((newByte >> 1) | (newByte << (7)));
            }

            if (lastByte)
            {
                newByte %= 128;
                newByte |= 1 << 6;
            }

            return newByte;
        }

        private static BigInteger getNumber(int bytesCout)
        {
            byte[] numBytes = new byte[bytesCout];
            for (int index = 0; index < bytesCout; index++)
            {
                numBytes[index] = getByte(index == bytesCout - 1);
            }
            return new BigInteger(numBytes);
        }

        public static bool MillerRabinTest(BigInteger number)
        {
            int round = (int)Math.Ceiling(BigInteger.Log(number, 2));
            if (number == 1 || number == 2 || number == 3)
                return true;
            if (number < 2 || number % 2 == 0)
                return false;

            BigInteger t = number - 1;
            int s = 0;

            while (t % 2 == 0)
            {
                t /= 2;
                s += 1;
            }

            for (int index = 0; index < round; index++)
            {
                BigInteger a;
                do
                {
                    a = getNumber(number.ToByteArray().Length - 1);
                }
                while (a < 2 || a > number - 2);

                BigInteger x = FastExpLib.lib.calculation(a, t, number);

                if (x == 1 || x == (number - 1))
                    continue;

                for (int i = 0; i < (s - 1); i++)
                {
                    x = FastExpLib.lib.calculation(x, 2, number);
                    if (x == 1)
                        return false;
                    if (x == (number - 1))
                        break;
                }

                if (x != number - 1)
                    return false;
            }
            return true;
        }

        public static BigInteger generate(int bitsCout)
        {
            BigInteger number = getNumber(bitsCout / 8);
            while(!MillerRabinTest(number))
                number++;
            return number;
        }
    }
}