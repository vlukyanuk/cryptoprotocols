﻿using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Numerics;
using System.Security.Cryptography;
using Newtonsoft.Json;

namespace Alice
{
    public partial class Form1 : Form
    {
        private Chat chat;
        private string rmAddress;
        private int rmPort;
        private int lcPort;
        private BigInteger sessionKey;
        private BigInteger randomNumber;
        private Thread receiveThread;
        UdpClient client;
        IPEndPoint remoteIp;

        public Form1()
        {
            InitializeComponent();
        }

        private void CheckFields()
        {
            try
            {
                rmAddress = "127.0.0.1";
                rmPort = Int32.Parse(remotePort.Text);
                lcPort = Int32.Parse(localPort.Text);
            }
            catch
            {
                rmAddress = "127.0.0.1";
                rmPort = 8080;
                remotePort.Text = 8080.ToString();
                lcPort = 8081;
                localPort.Text = 8081.ToString();
                PrintMessage("When you try to write errors fields, it was replaced to correct.");
            }
            finally
            {
                client = new UdpClient(lcPort);
                remoteIp = null;
            }
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            CheckFields();
            chat = new Chat(
                rmAddress,
                rmPort,
                lcPort,
                client
                );

            receiveThread = new Thread
                (
                    new ThreadStart(ReceiveMessage)
                );
            receiveThread.IsBackground = true;

            PrintMessage("Connection is active...");
            connectButton.Enabled = false;
            startProtocolButton.Enabled = true;
        }

        private void ReceiveMessage()
        {
            try
            {
                while (true)
                {
                    string message = EncryptDecryptMessage(chat.ReceiveString(client, remoteIp));
                    PrintMessage(message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed connection.");
            }
        }

        private void PrintMessage(string message)
        {
            messagesTextBox.Invoke
                (
                    new Action
                    (
                        () => messagesTextBox.Text = String.Concat
                        (
                            messagesTextBox.Text,
                            Environment.NewLine,
                            message
                        )
                    )
                );
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            chat.SendMessage(EncryptDecryptMessage(sendMessageTextBox.Text));
            PrintMessage(sendMessageTextBox.Text);
        }

        private void startProtocolButton_Click(object sender, EventArgs e)
        {
            randomNumber = SimpleNumberLib.lib.generate(128);
            RSAParameters privateKey = RSA.GetPrivateKey();
            RSAParameters publicKey = RSA.GetPublicKey();

            string json = JsonConvert.SerializeObject(publicKey);
            chat.SendMessage(json);
            json = chat.ReceiveString(client, remoteIp);
            RSAParameters bobPublicKey
                = JsonConvert.DeserializeObject<RSAParameters>(json);

            byte[] encryptSimpleNumber
                = RSA.RSAEncrypt
                (
                    randomNumber.ToByteArray(),
                    bobPublicKey,
                    false
                );

            int lengthOfEncSimpNumb = encryptSimpleNumber.Length / 2;
            byte[] firstHalfSimpleNumber
                = encryptSimpleNumber
                .Take(lengthOfEncSimpNumb)
                .ToArray();
            byte[] secondHalfOfSimpleNumber
                = encryptSimpleNumber
                .Skip(lengthOfEncSimpNumb)
                .ToArray();

            chat.SendMessage(firstHalfSimpleNumber);
            byte[] bobFirstHalfSimpleNumber
                = chat.ReceiveBytes(client, remoteIp);

            chat.SendMessage(secondHalfOfSimpleNumber);
            byte[] bobSecondHalfSimpleNumber
                = chat.ReceiveBytes(client, remoteIp);

            byte[] bobEncSimpNumber
                = new byte
                [
                    bobFirstHalfSimpleNumber.Length +
                    bobSecondHalfSimpleNumber.Length
                ];
            bobFirstHalfSimpleNumber
                .CopyTo(bobEncSimpNumber, 0);
            bobSecondHalfSimpleNumber
                .CopyTo(bobEncSimpNumber, bobFirstHalfSimpleNumber.Length);

            byte[] decryptBobSimpleNumber
                = RSA.RSADecrypt
                (
                    bobEncSimpNumber,
                    privateKey,
                    false
                );

            BigInteger bobSimpleNumber = new BigInteger(decryptBobSimpleNumber);

            sessionKey = randomNumber ^ bobSimpleNumber;

            startProtocolButton.Enabled = false;
            sendButton.Enabled = true;
            receiveThread.Start();
        }

        private string EncryptDecryptMessage(string message)
        {
            byte[] sessionBytes = sessionKey.ToByteArray();
            byte[] messageBytes = Encoding.Default.GetBytes(message);
            for (int i = 0; i < messageBytes.Length; i++)
            {
                messageBytes[i] = (byte)(messageBytes[i] ^ sessionBytes[i % sessionBytes.Length]);
            }
            return Encoding.Default.GetString(messageBytes);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
