﻿using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Numerics;
using System.Security.Cryptography;
using Newtonsoft.Json;
using Alice;

namespace Bob
{
    public partial class Form1 : Form
    {
        private Chat chat;
        private string rmAddress;
        private int rmPort;
        private int lcPort;
        private BigInteger sessionKey;
        private BigInteger randomNumber;
        private Thread receiveThread;
        UdpClient client;
        IPEndPoint remoteIp;

        public Form1()
        {
            InitializeComponent();
        }

        private void CheckFields()
        {
            try
            {
                rmAddress = "127.0.0.1";
                rmPort = Int32.Parse(remotePort.Text);
                lcPort = Int32.Parse(localPort.Text);
            }
            catch
            {
                rmAddress = "127.0.0.1";
                rmPort = 8081;
                remotePort.Text = 8081.ToString();
                lcPort = 8080;
                localPort.Text = 8080.ToString();
                PrintMessage("When you try to write errors fields, it was replaced to correct.");
            }
            finally
            {
                client = new UdpClient(lcPort);
                remoteIp = null;
            }
        }

        private void PrintMessage(string message)
        {
            messagesTextBox.Invoke
                (
                    new Action
                    (
                        () => messagesTextBox.Text = String.Concat
                        (
                            messagesTextBox.Text,
                            Environment.NewLine,
                            message
                        )
                    )
                );
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            CheckFields();
            chat = new Chat(
                rmAddress,
                rmPort,
                lcPort,
                client
                );

            receiveThread = new Thread
                (
                    new ThreadStart(ReceiveMessage)
                );
            receiveThread.IsBackground = true;

            PrintMessage("Connection is active...");
            connectButton.Enabled = false;

            AcceptProtocol();
        }

        private void AcceptProtocol()
        {
            randomNumber = SimpleNumberLib.lib.generate(128);
            RSAParameters privateKey = Alice.RSA.GetPrivateKey();
            RSAParameters publicKey = Alice.RSA.GetPublicKey();
            string json;

            json = chat.ReceiveString(client, remoteIp);
            RSAParameters alicePublicKey
                = JsonConvert.DeserializeObject<RSAParameters>(json);
            json = JsonConvert.SerializeObject(publicKey);
            chat.SendMessage(json);

            byte[] encryptSimpleNumber
                = Alice.RSA.RSAEncrypt
                (
                    randomNumber.ToByteArray(),
                    alicePublicKey,
                    false
                );

            int lengthOfEncSimpNumb = encryptSimpleNumber.Length / 2;
            byte[] firstHalfSimpleNumber
                = encryptSimpleNumber
                .Take(lengthOfEncSimpNumb)
                .ToArray();
            byte[] secondHalfOfSimpleNumber
                = encryptSimpleNumber
                .Skip(lengthOfEncSimpNumb)
                .ToArray();

            byte[] aliceFirstHalfSimpleNumber
                = chat.ReceiveBytes(client, remoteIp);
            chat.SendMessage(firstHalfSimpleNumber);

            byte[] aliceSecondHalfSimpleNumber
                = chat.ReceiveBytes(client, remoteIp);
            chat.SendMessage(secondHalfOfSimpleNumber);

            byte[] aliceEncSimpNumber
                = new byte
                [
                    aliceFirstHalfSimpleNumber.Length +
                    aliceSecondHalfSimpleNumber.Length
                ];
            aliceFirstHalfSimpleNumber
                .CopyTo(aliceEncSimpNumber, 0);
            aliceSecondHalfSimpleNumber
                .CopyTo(aliceEncSimpNumber, aliceFirstHalfSimpleNumber.Length);

            byte[] decryptAliceSimpleNumber
                = Alice.RSA.RSADecrypt
                (
                    aliceEncSimpNumber,
                    privateKey,
                    false
                );

            BigInteger aliceSimpleNumber = new BigInteger(decryptAliceSimpleNumber);

            sessionKey = randomNumber ^ aliceSimpleNumber;
            sendButton.Enabled = true;
            receiveThread.Start();

        }
        
        private void ReceiveMessage()
        {
            try
            {
                while (true)
                {
                    string message = EncryptDecryptMessage(chat.ReceiveString(client, remoteIp));
                    PrintMessage(message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed connection.");
            }
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            chat.SendMessage(EncryptDecryptMessage(sendMessageTextBox.Text));
            PrintMessage(sendMessageTextBox.Text);
        }

        private string EncryptDecryptMessage(string message)
        {
            byte[] sessionBytes = sessionKey.ToByteArray();
            byte[] messageBytes = Encoding.Default.GetBytes(message);
            for (int i = 0; i < messageBytes.Length; i++)
            {
                messageBytes[i] = (byte)(messageBytes[i] ^ sessionBytes[i % sessionBytes.Length]);
            }
            return Encoding.Default.GetString(messageBytes);
        }
    }
}
