﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Alice
{
    class Chat
    {
        private UdpClient client;
        private string remoteAddress;
        private int remotePort;
        private int localPort;

        public Chat
        (
            string rmAddress,
            int rmPort,
            int lcPort,
            UdpClient client
        )
        {
            remoteAddress = rmAddress;
            remotePort = rmPort;
            localPort = lcPort;
            this.client = client;
        }

        public void CloseConnection()
        {
            client.Close();
        }

        public void SendMessage(byte[] message)
        {
            client.Send
            (
                message,
                message.Length,
                remoteAddress,
                remotePort
            );
        }

        public void SendMessage(string message)
        {
            byte[] messageBytes = Encoding.Unicode.GetBytes(message);
            SendMessage(messageBytes);
        }

        public string ReceiveString
        (
            UdpClient receiver,
            IPEndPoint remoteIp
        )
        {
            byte[] data = receiver.Receive(ref remoteIp);
            return Encoding.Unicode.GetString(data);
        }

        public byte[] ReceiveBytes
            (
            UdpClient receiver,
            IPEndPoint remoteIp
        )
        {
            return receiver.Receive(ref remoteIp);
        }
    }
}
