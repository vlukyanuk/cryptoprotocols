﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Models
{
    class Response
    {
        public int Iteration { get; set; }
        public byte[] PayLoad { get; set; }
    }
}
