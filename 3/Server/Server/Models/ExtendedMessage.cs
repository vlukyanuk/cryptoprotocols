﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Server.Models
{
    class ExtendedMessage
    {
        public int Iteration { get; set; }
        public string User1 { get; set; }
        public string User2 { get; set; }
        public byte[] PayLoad1 { get; set; }
        public byte[] PayLoad2 { get; set; }
    }
}