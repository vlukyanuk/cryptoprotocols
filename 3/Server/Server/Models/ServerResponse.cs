﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Models
{
    class ServerResponse
    {
        public int Iteration { get; set; }
        public byte[] PayLoad1 { get; set; }
        public byte[] PayLoad2 { get; set; }
    }
}
