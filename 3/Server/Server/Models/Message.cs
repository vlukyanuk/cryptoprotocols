﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Models
{
    class Message
    {
        public int Iteration { get; set; }
        public string User1 { get; set; }
        public string User2 { get; set; }
        public byte[] PayLoad { get; set; }
    }
}
