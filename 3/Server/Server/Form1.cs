﻿using System;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Linq;
using Server.Models;
using Newtonsoft.Json;
using Message = Server.Models.Message;

namespace Server
{
    public partial class trentForm : Form
    {
        private Chat chat;
        private Chat server;
        private string rmAddress;
        private int aPort;
        private int bPort;
        private int srPort;
        private byte[] sessionkey;
        private byte[] key_a;
        private byte[] key_b;
        UdpClient client;
        IPEndPoint remoteIp;

        public trentForm()
        {
            InitializeComponent();
        }

        public void CheckFeld()
        {
            try
            {
                if (K_B.Text.Length < 1)
                    throw new FormatException();
                key_b = Encoding.Default.GetBytes(K_B.Text);
                if (K_A.Text.Length < 1)
                    throw new FormatException();
                key_a = Encoding.Default.GetBytes(K_A.Text);
                rmAddress = "127.0.0.1";
                aPort = Int32.Parse(alicePort.Text);
                bPort = Int32.Parse(bobPort.Text);
                srPort = Int32.Parse(trent.Text);
            }
            catch
            {
                K_B.Text = "1872dhksdfh43f7!#@dkd";
                key_b = Encoding.Default.GetBytes(K_B.Text);
                K_A.Text = "gd936dagfc080732b!bs@#";
                key_a = Encoding.Default.GetBytes(K_A.Text);
                aPort = 8080;
                bPort = 8081;
                srPort = 8082;
            }
            finally
            {
                client = new UdpClient(srPort);
                remoteIp = null;
            }
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            CheckFeld();
            chat = new Chat(
                rmAddress,
                bPort,
                srPort,
                client
                );

            MessageBox.Show("Connection is active...");
            connectButton.Enabled = false;
            startProtocolButton.Enabled = true;
        }

        private string EncryptDecryptMessage(string message, byte[] key)
        {
            byte[] sessionBytes = key;
            byte[] messageBytes = Encoding.Default.GetBytes(message);
            for (int i = 0; i < messageBytes.Length; i++)
            {
                messageBytes[i] = (byte)(messageBytes[i] ^ sessionBytes[i % sessionBytes.Length]);
            }
            return Encoding.Default.GetString(messageBytes);
        }

        private string RandomString(int length)
        {
            Random random = new Random();
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private void startProtocolButton_Click(object sender, EventArgs e)
        {
            byte[] sessionKey
                = Encoding.Default.GetBytes(RandomString(32));

            string json = chat.ReceiveString(client, remoteIp);

            ExtendedMessage messageFromBob
                = JsonConvert.DeserializeObject<ExtendedMessage>(json);

            json = EncryptDecryptMessage
                (
                    Encoding.Default.GetString
                    (
                        messageFromBob.PayLoad1
                    ),
                    key_a
                );

            Message aliceMessage
                = JsonConvert.DeserializeObject<Message>(json);

            json
                = JsonConvert.SerializeObject
                (
                    new Response()
                    {
                        Iteration = BitConverter.ToInt32(aliceMessage.PayLoad, 0),
                        PayLoad = sessionKey
                    }
                );

            string m1 = EncryptDecryptMessage(json, key_a);

            json = EncryptDecryptMessage
                (
                    Encoding.Default.GetString
                    (
                        messageFromBob.PayLoad2
                    ),
                    key_b
                );

            Message bobMessage
                = JsonConvert.DeserializeObject<Message>(json);

            json
                = JsonConvert.SerializeObject
                (
                    new Response()
                    {
                        Iteration = BitConverter.ToInt32(bobMessage.PayLoad, 0),
                        PayLoad = sessionKey
                    }
                );

            string m2 = EncryptDecryptMessage(json, key_b);

            json = JsonConvert.SerializeObject
                (
                    new ServerResponse()
                    {
                        Iteration = 3,
                        PayLoad1 = Encoding.Default.GetBytes(m1),
                        PayLoad2 = Encoding.Default.GetBytes(m2)
                    }
                );

            chat.SendMessage(json);

            Application.Exit();
        }

    }
}
