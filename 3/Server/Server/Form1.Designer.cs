﻿namespace Server
{
    partial class trentForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.connectButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.K_B = new System.Windows.Forms.TextBox();
            this.startProtocolButton = new System.Windows.Forms.Button();
            this.trent = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.alicePort = new System.Windows.Forms.TextBox();
            this.bobPort = new System.Windows.Forms.TextBox();
            this.K_A = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(345, 58);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(100, 23);
            this.connectButton.TabIndex = 44;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(285, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 43;
            this.label4.Text = "K_B";
            // 
            // K_B
            // 
            this.K_B.Enabled = false;
            this.K_B.Location = new System.Drawing.Point(7, 33);
            this.K_B.Name = "K_B";
            this.K_B.Size = new System.Drawing.Size(272, 20);
            this.K_B.TabIndex = 42;
            this.K_B.Text = "1872dhksdfh43f7!#@dkd";
            // 
            // startProtocolButton
            // 
            this.startProtocolButton.Enabled = false;
            this.startProtocolButton.Location = new System.Drawing.Point(345, 31);
            this.startProtocolButton.Name = "startProtocolButton";
            this.startProtocolButton.Size = new System.Drawing.Size(100, 23);
            this.startProtocolButton.TabIndex = 41;
            this.startProtocolButton.Text = "Start Protocol";
            this.startProtocolButton.UseVisualStyleBackColor = true;
            this.startProtocolButton.Click += new System.EventHandler(this.startProtocolButton_Click);
            // 
            // trent
            // 
            this.trent.Enabled = false;
            this.trent.Location = new System.Drawing.Point(285, 9);
            this.trent.Name = "trent";
            this.trent.Size = new System.Drawing.Size(54, 20);
            this.trent.TabIndex = 40;
            this.trent.Text = "8082";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(251, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 39;
            this.label3.Text = "trent";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(124, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 38;
            this.label2.Text = "Alice port";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 37;
            this.label1.Text = "Bob port";
            // 
            // alicePort
            // 
            this.alicePort.Enabled = false;
            this.alicePort.Location = new System.Drawing.Point(190, 9);
            this.alicePort.Name = "alicePort";
            this.alicePort.Size = new System.Drawing.Size(54, 20);
            this.alicePort.TabIndex = 36;
            this.alicePort.Text = "8080";
            // 
            // bobPort
            // 
            this.bobPort.Enabled = false;
            this.bobPort.Location = new System.Drawing.Point(64, 9);
            this.bobPort.Name = "bobPort";
            this.bobPort.Size = new System.Drawing.Size(54, 20);
            this.bobPort.TabIndex = 35;
            this.bobPort.Text = "8081";
            // 
            // K_A
            // 
            this.K_A.Enabled = false;
            this.K_A.Location = new System.Drawing.Point(7, 60);
            this.K_A.Name = "K_A";
            this.K_A.Size = new System.Drawing.Size(272, 20);
            this.K_A.TabIndex = 45;
            this.K_A.Text = "gd936dagfc080732b!bs@#";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(285, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 46;
            this.label5.Text = "K_A";
            // 
            // trentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 90);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.K_A);
            this.Controls.Add(this.connectButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.K_B);
            this.Controls.Add(this.startProtocolButton);
            this.Controls.Add(this.trent);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.alicePort);
            this.Controls.Add(this.bobPort);
            this.Name = "trentForm";
            this.Text = "Trent";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox K_B;
        private System.Windows.Forms.Button startProtocolButton;
        private System.Windows.Forms.TextBox trent;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox alicePort;
        private System.Windows.Forms.TextBox bobPort;
        private System.Windows.Forms.TextBox K_A;
        private System.Windows.Forms.Label label5;
    }
}

