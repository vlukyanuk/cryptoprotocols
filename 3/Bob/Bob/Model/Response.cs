﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bob.Models
{
    class Response
    {
        public int Iteration { get; set; }
        public byte[] PayLoad { get; set; }
    }
}
