﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bob.Models
{
    class BobMessage
    {
        public int Iteration { get; set; }
        public string User1 { get; set; }
        public string User2 { get; set; }
        public byte[] PayLoad { get; set; }
    }
}
