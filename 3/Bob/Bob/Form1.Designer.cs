﻿namespace Bob
{
    partial class Bob
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.connectButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.K_B = new System.Windows.Forms.TextBox();
            this.startProtocolButton = new System.Windows.Forms.Button();
            this.trent = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.sendMessageTextBox = new System.Windows.Forms.TextBox();
            this.sendButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.messagesTextBox = new System.Windows.Forms.TextBox();
            this.remotePort = new System.Windows.Forms.TextBox();
            this.localPort = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(350, 36);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(100, 23);
            this.connectButton.TabIndex = 34;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(290, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 33;
            this.label4.Text = "K_B";
            // 
            // K_B
            // 
            this.K_B.Enabled = false;
            this.K_B.Location = new System.Drawing.Point(12, 38);
            this.K_B.Name = "K_B";
            this.K_B.Size = new System.Drawing.Size(272, 20);
            this.K_B.TabIndex = 32;
            this.K_B.Text = "1872dhksdfh43f7!#@dkd";
            // 
            // startProtocolButton
            // 
            this.startProtocolButton.Enabled = false;
            this.startProtocolButton.Location = new System.Drawing.Point(350, 12);
            this.startProtocolButton.Name = "startProtocolButton";
            this.startProtocolButton.Size = new System.Drawing.Size(100, 23);
            this.startProtocolButton.TabIndex = 31;
            this.startProtocolButton.Text = "Start Protocol";
            this.startProtocolButton.UseVisualStyleBackColor = true;
            this.startProtocolButton.Click += new System.EventHandler(this.startProtocolButton_Click);
            // 
            // trent
            // 
            this.trent.Enabled = false;
            this.trent.Location = new System.Drawing.Point(290, 14);
            this.trent.Name = "trent";
            this.trent.Size = new System.Drawing.Size(54, 20);
            this.trent.TabIndex = 30;
            this.trent.Text = "8082";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(256, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 29;
            this.label3.Text = "trent";
            // 
            // sendMessageTextBox
            // 
            this.sendMessageTextBox.Enabled = false;
            this.sendMessageTextBox.Location = new System.Drawing.Point(12, 237);
            this.sendMessageTextBox.Name = "sendMessageTextBox";
            this.sendMessageTextBox.Size = new System.Drawing.Size(357, 20);
            this.sendMessageTextBox.TabIndex = 28;
            // 
            // sendButton
            // 
            this.sendButton.Enabled = false;
            this.sendButton.Location = new System.Drawing.Point(375, 235);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(75, 23);
            this.sendButton.TabIndex = 27;
            this.sendButton.Text = "Send";
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.sendButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(129, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 26;
            this.label2.Text = "remote port";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "local port";
            // 
            // messagesTextBox
            // 
            this.messagesTextBox.Location = new System.Drawing.Point(12, 63);
            this.messagesTextBox.Multiline = true;
            this.messagesTextBox.Name = "messagesTextBox";
            this.messagesTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.messagesTextBox.Size = new System.Drawing.Size(438, 166);
            this.messagesTextBox.TabIndex = 24;
            // 
            // remotePort
            // 
            this.remotePort.Enabled = false;
            this.remotePort.Location = new System.Drawing.Point(195, 14);
            this.remotePort.Name = "remotePort";
            this.remotePort.Size = new System.Drawing.Size(54, 20);
            this.remotePort.TabIndex = 23;
            this.remotePort.Text = "8080";
            // 
            // localPort
            // 
            this.localPort.Enabled = false;
            this.localPort.Location = new System.Drawing.Point(69, 14);
            this.localPort.Name = "localPort";
            this.localPort.Size = new System.Drawing.Size(54, 20);
            this.localPort.TabIndex = 22;
            this.localPort.Text = "8081";
            // 
            // Bob
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 271);
            this.Controls.Add(this.connectButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.K_B);
            this.Controls.Add(this.startProtocolButton);
            this.Controls.Add(this.trent);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.sendMessageTextBox);
            this.Controls.Add(this.sendButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.messagesTextBox);
            this.Controls.Add(this.remotePort);
            this.Controls.Add(this.localPort);
            this.Name = "Bob";
            this.Text = "Bob";
            this.Load += new System.EventHandler(this.Bob_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox K_B;
        private System.Windows.Forms.Button startProtocolButton;
        private System.Windows.Forms.TextBox trent;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox sendMessageTextBox;
        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox messagesTextBox;
        private System.Windows.Forms.TextBox remotePort;
        private System.Windows.Forms.TextBox localPort;
    }
}

