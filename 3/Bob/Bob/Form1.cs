﻿using System;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Newtonsoft.Json;
using Bob.Models;


namespace Bob
{
    public partial class Bob : Form
    {

        private Chat chat;
        private Chat server;
        private string rmAddress;
        private int rmPort;
        private int lcPort;
        private int srPort;
        private byte[] sessionkey;
        private byte[] key_b;
        private Thread receiveThread;
        UdpClient client;
        IPEndPoint remoteIp;

        public Bob()
        {
            InitializeComponent();
        }

        private void Bob_Load(object sender, EventArgs e)
        {

        }

        public void CheckFeld()
        {
            try
            {
                if (K_B.Text.Length < 1)
                    throw new FormatException();
                key_b = Encoding.Default.GetBytes(K_B.Text);
                rmAddress = "127.0.0.1";
                rmPort = Int32.Parse(remotePort.Text);
                lcPort = Int32.Parse(localPort.Text);
                srPort = Int32.Parse(trent.Text);
            }
            catch
            {
                K_B.Text = "1872dhksdfh43f7!#@dkd";
                key_b = Encoding.Default.GetBytes(K_B.Text);
                rmPort = 8080;
                lcPort = 8081;
                srPort = 8082;
            }
            finally
            {
                client = new UdpClient(lcPort);
                remoteIp = null;
            }
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            chat.SendMessage(EncryptDecryptMessage(sendMessageTextBox.Text, sessionkey));
            PrintMessage(sendMessageTextBox.Text);
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            CheckFeld();
            chat = new Chat(
                rmAddress,
                rmPort,
                lcPort,
                client
                );

            server = new Chat(
                rmAddress,
                srPort,
                lcPort,
                client
                );

            receiveThread = new Thread
                (
                    new ThreadStart(ReceiveMessage)
                );
            receiveThread.IsBackground = true;

            PrintMessage("Connection is active...");
            connectButton.Enabled = false;
            startProtocolButton.Enabled = true;
        }

        private void ReceiveMessage()
        {
            try
            {
                while (true)
                {
                    string message = EncryptDecryptMessage(chat.ReceiveString(client, remoteIp), sessionkey);
                    PrintMessage(message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed connection.");
            }
        }

        private void PrintMessage(string message)
        {
            messagesTextBox.Invoke
                (
                    new Action
                    (
                        () => messagesTextBox.Text = String.Concat
                        (
                            messagesTextBox.Text,
                            Environment.NewLine,
                            message
                        )
                    )
                );
        }

        private string EncryptDecryptMessage(string message, byte[] key)
        {
            byte[] sessionBytes = key;
            byte[] messageBytes = Encoding.Default.GetBytes(message);
            for (int i = 0; i < messageBytes.Length; i++)
            {
                messageBytes[i] = (byte)(messageBytes[i] ^ sessionBytes[i % sessionBytes.Length]);
            }
            return Encoding.Default.GetString(messageBytes);
        }

        private void startProtocolButton_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            int randomNumber = rnd.Next(0, int.MaxValue);

            string json = JsonConvert.SerializeObject
                (
                 new BobMessage()
                 {
                     Iteration = 2,
                     User1 = "Alice",
                     User2 = "Bob",
                     PayLoad = BitConverter.GetBytes(randomNumber)
                 }
                );

            json = EncryptDecryptMessage(json, key_b);

            string aliceJson = chat.ReceiveString(client, remoteIp);

            BobMessage aliceMessage = JsonConvert.DeserializeObject<BobMessage>(aliceJson);
            
            json = JsonConvert.SerializeObject
                (
                new ExtendedBobMessage()
                {
                    Iteration = 2,
                    User1 = "Alice",
                    User2 = "Bob",
                    PayLoad1 = aliceMessage.PayLoad,
                    PayLoad2 = Encoding.Default.GetBytes(json)
                }
                );

            server.SendMessage(json);

            json = server.ReceiveString(client, remoteIp);

            ServerResponse serverResp = JsonConvert
                .DeserializeObject<ServerResponse>(json);

            json = EncryptDecryptMessage
                (
                    Encoding.Default.GetString(serverResp.PayLoad2), key_b
                );

            Response resp = JsonConvert
                .DeserializeObject<Response>(json);

            int newRandom = resp.Iteration;

            if (randomNumber != newRandom)
                MessageBox.Show("Malary is here!!!");

            sessionkey = resp.PayLoad;

            json = JsonConvert.SerializeObject
                (
                    new Response()
                    {
                        Iteration = 4,
                        PayLoad = serverResp.PayLoad1
                    }
                );

            chat.SendMessage(json);

            startProtocolButton.Enabled = false;
            sendButton.Enabled = true;
            sendMessageTextBox.Enabled = true;
            receiveThread.Start();
        }
    }
}
