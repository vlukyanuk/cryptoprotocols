﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Security.Cryptography;
using Newtonsoft.Json;
using Alice.Models;

namespace Alice
{
    public partial class Alice : Form
    {
        private Chat chat;
        private Chat server;
        private string rmAddress;
        private int rmPort;
        private int lcPort;
        private int srPort;
        private byte[] sessionkey;
        private byte[] key_a;
        private Thread receiveThread;
        UdpClient client;
        IPEndPoint remoteIp;

        public Alice()
        {
            InitializeComponent();
        }

        public void CheckFeld()
        {
            try
            {
                if (K_A.Text.Length < 1)
                    throw new FormatException();
                key_a = Encoding.Default.GetBytes(K_A.Text);
                rmAddress = "127.0.0.1";
                rmPort = Int32.Parse(remotePort.Text);
                lcPort = Int32.Parse(localPort.Text);
                srPort = Int32.Parse(trent.Text);
            }
            catch
            {
                K_A.Text = "gd936dagfc080732b!bs@#";
                key_a = Encoding.Default.GetBytes(K_A.Text);
                rmPort = 8081;
                lcPort = 8080;
                srPort = 8082;
            }
            finally
            {
                client = new UdpClient(lcPort);
                remoteIp = null;
            }
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            chat.SendMessage(EncryptDecryptMessage(sendMessageTextBox.Text, sessionkey));
            PrintMessage(sendMessageTextBox.Text);
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            CheckFeld();
            chat = new Chat(
                rmAddress,
                rmPort,
                lcPort,
                client
                );

            server = new Chat(
                rmAddress,
                srPort,
                lcPort,
                client
                );

            receiveThread = new Thread
                (
                    new ThreadStart(ReceiveMessage)
                );
            receiveThread.IsBackground = true;

            PrintMessage("Connection is active...");
            connectButton.Enabled = false;
            startProtocolButton.Enabled = true;
        }

        private void ReceiveMessage()
        {
            try
            {
                while (true)
                {
                    string message = EncryptDecryptMessage(chat.ReceiveString(client, remoteIp), sessionkey);
                    PrintMessage(message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed connection.");
            }
        }

        private void PrintMessage(string message)
        {
            messagesTextBox.Invoke
                (
                    new Action
                    (
                        () => messagesTextBox.Text = String.Concat
                        (
                            messagesTextBox.Text,
                            Environment.NewLine,
                            message
                        )
                    )
                );
        }

        private string EncryptDecryptMessage(string message, byte[] key)
        {
            byte[] sessionBytes = key;
            byte[] messageBytes = Encoding.Default.GetBytes(message);
            for (int i = 0; i < messageBytes.Length; i++)
            {
                messageBytes[i] = (byte)(messageBytes[i] ^ sessionBytes[i % sessionBytes.Length]);
            }
            return Encoding.Default.GetString(messageBytes);
        }

        private void startProtocolButton_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            int randomNumber = rnd.Next(0, int.MaxValue);

            string json = JsonConvert.SerializeObject
                (
                 new AliceMessage()
                 {
                     Iteration = 1,
                     User1 = "Alice",
                     User2 = "Bob",
                     PayLoad = BitConverter.GetBytes(randomNumber)
                 }
                );

            json = EncryptDecryptMessage(json, key_a);

            json = JsonConvert.SerializeObject
                (
                new AliceMessage()
                {
                    Iteration = 1,
                    User1 = "Alice",
                    User2 = "Bob",
                    PayLoad = Encoding.Default.GetBytes(json)
                }
                );

            chat.SendMessage(json);

            json = chat.ReceiveString(client, remoteIp);

            Response resp = JsonConvert
                .DeserializeObject<Response>(json);

            json = EncryptDecryptMessage
                (
                    Encoding.Default.GetString(resp.PayLoad), key_a
                );

            resp = JsonConvert
                .DeserializeObject<Response>(json);

            int newRandom = resp.Iteration;

            if (randomNumber != newRandom)
                MessageBox.Show("Malary is here!!!");

            sessionkey = resp.PayLoad;

            startProtocolButton.Enabled = false;
            sendButton.Enabled = true;
            sendMessageTextBox.Enabled = true;
            receiveThread.Start();

        }
    }
}
