﻿namespace General2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.connectButton = new System.Windows.Forms.Button();
            this.p1Message = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.p4Answer = new System.Windows.Forms.TextBox();
            this.p3Answer = new System.Windows.Forms.TextBox();
            this.p2Answer = new System.Windows.Forms.TextBox();
            this.p1Answer = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.p4TextBox = new System.Windows.Forms.TextBox();
            this.p3TextBox = new System.Windows.Forms.TextBox();
            this.p2TextBox = new System.Windows.Forms.TextBox();
            this.p1TextBox = new System.Windows.Forms.TextBox();
            this.p4Message = new System.Windows.Forms.TextBox();
            this.p3Message = new System.Windows.Forms.TextBox();
            this.p2Message = new System.Windows.Forms.TextBox();
            this.sendButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // connectButton
            // 
            this.connectButton.BackColor = System.Drawing.Color.Red;
            this.connectButton.Location = new System.Drawing.Point(17, 155);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75, 23);
            this.connectButton.TabIndex = 38;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = false;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // p1Message
            // 
            this.p1Message.Location = new System.Drawing.Point(17, 15);
            this.p1Message.Name = "p1Message";
            this.p1Message.Size = new System.Drawing.Size(100, 20);
            this.p1Message.TabIndex = 37;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(246, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 36;
            this.label5.Text = "Answer";
            // 
            // p4Answer
            // 
            this.p4Answer.Location = new System.Drawing.Point(180, 129);
            this.p4Answer.Name = "p4Answer";
            this.p4Answer.Size = new System.Drawing.Size(48, 20);
            this.p4Answer.TabIndex = 35;
            // 
            // p3Answer
            // 
            this.p3Answer.Location = new System.Drawing.Point(126, 129);
            this.p3Answer.Name = "p3Answer";
            this.p3Answer.Size = new System.Drawing.Size(48, 20);
            this.p3Answer.TabIndex = 34;
            // 
            // p2Answer
            // 
            this.p2Answer.Location = new System.Drawing.Point(72, 129);
            this.p2Answer.Name = "p2Answer";
            this.p2Answer.Size = new System.Drawing.Size(48, 20);
            this.p2Answer.TabIndex = 33;
            // 
            // p1Answer
            // 
            this.p1Answer.Location = new System.Drawing.Point(17, 129);
            this.p1Answer.Name = "p1Answer";
            this.p1Answer.Size = new System.Drawing.Size(48, 20);
            this.p1Answer.TabIndex = 32;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(123, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 13);
            this.label4.TabIndex = 31;
            this.label4.Text = "Message             Port";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(123, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "Message             Port";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(123, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 13);
            this.label2.TabIndex = 29;
            this.label2.Text = "Message             Port";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(123, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "Real                    Port";
            // 
            // p4TextBox
            // 
            this.p4TextBox.Location = new System.Drawing.Point(237, 93);
            this.p4TextBox.Name = "p4TextBox";
            this.p4TextBox.Size = new System.Drawing.Size(63, 20);
            this.p4TextBox.TabIndex = 27;
            // 
            // p3TextBox
            // 
            this.p3TextBox.Location = new System.Drawing.Point(237, 67);
            this.p3TextBox.Name = "p3TextBox";
            this.p3TextBox.Size = new System.Drawing.Size(63, 20);
            this.p3TextBox.TabIndex = 26;
            // 
            // p2TextBox
            // 
            this.p2TextBox.Location = new System.Drawing.Point(237, 41);
            this.p2TextBox.Name = "p2TextBox";
            this.p2TextBox.Size = new System.Drawing.Size(63, 20);
            this.p2TextBox.TabIndex = 25;
            // 
            // p1TextBox
            // 
            this.p1TextBox.Location = new System.Drawing.Point(237, 15);
            this.p1TextBox.Name = "p1TextBox";
            this.p1TextBox.Size = new System.Drawing.Size(63, 20);
            this.p1TextBox.TabIndex = 24;
            // 
            // p4Message
            // 
            this.p4Message.Location = new System.Drawing.Point(17, 93);
            this.p4Message.Name = "p4Message";
            this.p4Message.Size = new System.Drawing.Size(100, 20);
            this.p4Message.TabIndex = 23;
            // 
            // p3Message
            // 
            this.p3Message.Location = new System.Drawing.Point(17, 67);
            this.p3Message.Name = "p3Message";
            this.p3Message.Size = new System.Drawing.Size(100, 20);
            this.p3Message.TabIndex = 22;
            // 
            // p2Message
            // 
            this.p2Message.Location = new System.Drawing.Point(17, 41);
            this.p2Message.Name = "p2Message";
            this.p2Message.Size = new System.Drawing.Size(100, 20);
            this.p2Message.TabIndex = 21;
            // 
            // sendButton
            // 
            this.sendButton.BackColor = System.Drawing.Color.Red;
            this.sendButton.Enabled = false;
            this.sendButton.Location = new System.Drawing.Point(225, 155);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(75, 23);
            this.sendButton.TabIndex = 20;
            this.sendButton.Text = "Send";
            this.sendButton.UseVisualStyleBackColor = false;
            this.sendButton.Click += new System.EventHandler(this.sendButton_Click_2);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.ClientSize = new System.Drawing.Size(335, 195);
            this.Controls.Add(this.connectButton);
            this.Controls.Add(this.p1Message);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.p4Answer);
            this.Controls.Add(this.p3Answer);
            this.Controls.Add(this.p2Answer);
            this.Controls.Add(this.p1Answer);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.p4TextBox);
            this.Controls.Add(this.p3TextBox);
            this.Controls.Add(this.p2TextBox);
            this.Controls.Add(this.p1TextBox);
            this.Controls.Add(this.p4Message);
            this.Controls.Add(this.p3Message);
            this.Controls.Add(this.p2Message);
            this.Controls.Add(this.sendButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.TextBox p1Message;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox p4Answer;
        private System.Windows.Forms.TextBox p3Answer;
        private System.Windows.Forms.TextBox p2Answer;
        private System.Windows.Forms.TextBox p1Answer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox p4TextBox;
        private System.Windows.Forms.TextBox p3TextBox;
        private System.Windows.Forms.TextBox p2TextBox;
        private System.Windows.Forms.TextBox p1TextBox;
        private System.Windows.Forms.TextBox p4Message;
        private System.Windows.Forms.TextBox p3Message;
        private System.Windows.Forms.TextBox p2Message;
        private System.Windows.Forms.Button sendButton;
    }
}

