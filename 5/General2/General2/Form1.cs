﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace General2
{
    public partial class Form1 : Form
    {
        private int p1;
        private int p2;
        private int p3;
        private int p4;
        private Chat p2Chat;
        private Chat p3Chat;
        private Chat p4Chat;
        UdpClient client;
        IPEndPoint remoteIp;
        private string rmAddress;

        public Form1()
        {
            InitializeComponent();
        }

        private bool checkFileds()
        {
            try
            {
                p1 = Int32.Parse(p1TextBox.Text);
                p2 = Int32.Parse(p2TextBox.Text);
                p3 = Int32.Parse(p3TextBox.Text);
                p4 = Int32.Parse(p4TextBox.Text);
                return true;
            }
            catch
            {
                p1TextBox.Text = "8080";
                p1 = 8080;
                p2TextBox.Text = "8081";
                p2 = 8081;
                p3TextBox.Text = "8082";
                p3 = 8082;
                p4TextBox.Text = "8083";
                p4 = 8083;
                return false;
            }
            finally
            {
                rmAddress = "127.0.0.1";
                client = (client != null) ? client : new UdpClient(p1);
                remoteIp = null;
            }
        }

        private void setConnect()
        {
            p2Chat = new Chat(
                rmAddress,
                p2,
                p1,
                client
                );

            p3Chat = new Chat(
                rmAddress,
                p3,
                p1,
                client
                );

            p4Chat = new Chat(
                rmAddress,
                p4,
                p1,
                client
                );
        }

        private void processing()
        {
            p2Chat.ReceiveString(client, remoteIp);
            string[] vector1 = new string[4];
            p2Chat.SendMessage(p2Message.Text);
            p3Chat.SendMessage(p3Message.Text);
            p4Chat.SendMessage(p4Message.Text);

            vector1[0] = p1Message.Text;
            vector1[1] = p2Chat.ReceiveString(client, remoteIp);
            vector1[2] = p3Chat.ReceiveString(client, remoteIp);
            vector1[3] = p4Chat.ReceiveString(client, remoteIp);

            string json = JsonConvert.SerializeObject(vector1);
            p2Chat.SendMessage(json);
            p3Chat.SendMessage(json);
            p4Chat.SendMessage(json);

            string[] vector2 = JsonConvert.DeserializeObject<string[]>
                (
                p2Chat.ReceiveString(client, remoteIp)
                );
            string[] vector3 = JsonConvert.DeserializeObject<string[]>
                (
                p3Chat.ReceiveString(client, remoteIp)
                );
            string[] vector4 = JsonConvert.DeserializeObject<string[]>
                (
                p4Chat.ReceiveString(client, remoteIp)
                );

            string[] answer = new string[]
            {
                getAnswer(vector2[0],vector3[0],vector4[0]),
                getAnswer(vector1[1],vector3[1],vector4[1]),
                getAnswer(vector1[2],vector2[2],vector4[2]),
                getAnswer(vector1[3],vector2[3],vector3[3])
            };

            p1Answer.Text = answer[0];
            p2Answer.Text = answer[1];
            p3Answer.Text = answer[2];
            p4Answer.Text = answer[3];
        }

        private string getAnswer(string v1, string v2, string v3)
        {
            string[] forSearch = new string[]
                {
                    v1,
                    v2,
                    v3
                };
            return forSearch
                .Where(str => forSearch.Count(s => s == str) > 1)
                .Distinct()
                .ToList()[0];
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            sendButton.Enabled = false;
            processing();
        }

        private void sendButton_Click_1(object sender, EventArgs e)
        {

        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            if (!checkFileds())
                return;
            setConnect();
            sendButton.Enabled = true;
        }

        private void sendButton_Click_2(object sender, EventArgs e)
        {
            sendButton.Enabled = false;
            processing();
        }
    }
}
