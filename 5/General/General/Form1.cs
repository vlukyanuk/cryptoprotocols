﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using General.Models;

namespace General
{
    public partial class General : Form
    {
        private int p1;
        private int p2;
        private int p3;
        private int p4;
        private Chat p2Chat;
        private Chat p3Chat;
        private Chat p4Chat;
        UdpClient client;
        IPEndPoint remoteIp;
        private string rmAddress;

        Thread t1;
        Thread t2;
        Thread t3;
        Thread validator;

        string[] bufVector;
        string[] vector1;
        string[] vector2;
        string[] vector3;
        string[] vector4;
        bool[,] execute;
        string pn;

        bool hadSend;

        int[] ports;

        public General()
        {
            InitializeComponent();
            bufVector = new string[4];
            execute = new bool[2, 3];
            hadSend = false;
        }

        private bool checkFileds()
        {
            try
            {
                p1 = Int32.Parse(p1TextBox.Text);
                p2 = Int32.Parse(p2TextBox.Text);
                p3 = Int32.Parse(p3TextBox.Text);
                p4 = Int32.Parse(p4TextBox.Text);
                return true;
            }
            catch
            {
                p1TextBox.Text = "8080";
                p1 = 8080;
                p2TextBox.Text = "8081";
                p2 = 8081;
                p3TextBox.Text = "8082";
                p3 = 8082;
                p4TextBox.Text = "8083";
                p4 = 8083;
                return false;
            }
            finally
            {
                rmAddress = "127.0.0.1";
                client = (client != null) ? client : new UdpClient(p1);
                remoteIp = null;
            }
        }

        private void setConnect()
        {
            p2Chat = new Chat(
                rmAddress,
                p2,
                p1,
                client
                );

            p3Chat = new Chat(
                rmAddress,
                p3,
                p1,
                client
                );

            p4Chat = new Chat(
                rmAddress,
                p4,
                p1,
                client
                );
        }

        private string getAnswer(string v1, string v2, string v3)
        {
            string[] forSearch = new string[]
                {
                    v1,
                    v2,
                    v3
                };
            string resp;
            try
            {
                resp = forSearch
                    .Where(str => forSearch.Count(s => s == str) > 1)
                    .Distinct()
                    .ToList()[0];
            }
            catch
            {
                resp = "none";
            }
            return resp;
        }

        private void sendMessage(Chat chat, int port, string message)
        {
            chat.SendMessage
                (
                    JsonConvert.SerializeObject
                    (
                        new Models.Message()
                        {
                            port = port,
                            message = message
                        }
                    )
                );
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            sendButton.Enabled = false;
            
            setValueToVector(p1, p1Message.Text);

            sendMessage(p2Chat, p1, p2Message.Text);
            sendMessage(p3Chat, p1, p3Message.Text);
            sendMessage(p4Chat, p1, p4Message.Text);

            hadSend = true;
        }

        private void validate()
        {
            while (true)
            {
                if (execute[0, 0] && execute[0, 1] && execute[0, 2] && hadSend)
                {
                    string json;
                    if (enemyCheckBox.Checked)
                    {
                        json = JsonConvert.SerializeObject
                            (
                                new string[]
                                {
                                    p1Message.Text,
                                    p2Message.Text,
                                    p3Message.Text,
                                    p4Message.Text
                                }
                            );
                    }
                    else
                    {
                        json = JsonConvert.SerializeObject(bufVector);
                    }

                    setVectorToVector(p1, bufVector);
                    sendMessage(p2Chat, p1, json);
                    sendMessage(p3Chat, p1, json);
                    sendMessage(p4Chat, p1, json);
                    break;
                }
                Thread.Sleep(500);
            }

            while (true)
            {
                if (execute[1, 0] && execute[1, 1] && execute[1, 2] && hadSend)
                {
                    string[] answer = new string[]
                        {
                            getAnswer(vector2[0],vector3[0],vector4[0]),
                            getAnswer(vector1[1],vector3[1],vector4[1]),
                            getAnswer(vector1[2],vector2[2],vector4[2]),
                            getAnswer(vector1[3],vector2[3],vector3[3])
                        };

                    printMessage(p1Answer, answer[0]);
                    printMessage(p2Answer, answer[1]);
                    printMessage(p3Answer, answer[2]);
                    printMessage(p4Answer, answer[3]);
                    break;
                }
                Thread.Sleep(500);
            }
        }

        private void printMessage(TextBox tb, string text)
        {
            tb.Invoke
                    (
                        new Action
                        (
                            () => tb.Text = text
                        )
                    );
        }

        private void setVectorToVector(int port, string[] value)
        {
            if (port == ports[0])
                vector1 = value;

            if (port == ports[1])
                vector2 = value;

            if (port == ports[2])
                vector3 = value;

            if (port == ports[3])
                vector4 = value;
        }

        private void setValueToVector(int port, string value)
        {
            for (int i = 0; i < 4; i++)
                if (port == ports[i])
                {
                    bufVector[i] = value;
                    return;
                }

        }

        private void firstReceiver()
        {
            string resp = p2Chat.ReceiveString(client, remoteIp);
            Models.Message m = JsonConvert
                .DeserializeObject<Models.Message>(resp);
            setValueToVector(m.port, m.message);
            execute[0, 0] = true;

            resp = p2Chat.ReceiveString(client, remoteIp);
            m = JsonConvert
                .DeserializeObject<Models.Message>(resp);
            setVectorToVector(m.port, JsonConvert.DeserializeObject<string[]>(m.message));
            execute[1, 0] = true;
        }

        private void secondReceiver()
        {
            string resp = p3Chat.ReceiveString(client, remoteIp);
            Models.Message m = JsonConvert
                .DeserializeObject<Models.Message>(resp);
            setValueToVector(m.port, m.message);
            execute[0, 1] = true;

            resp = p3Chat.ReceiveString(client, remoteIp);
            m = JsonConvert
                .DeserializeObject<Models.Message>(resp);
            setVectorToVector(m.port, JsonConvert.DeserializeObject<string[]>(m.message));
            execute[1, 1] = true;
        }

        private void trirdReceiver()
        {
            string resp = p4Chat.ReceiveString(client, remoteIp);
            Models.Message m = JsonConvert
                .DeserializeObject<Models.Message>(resp);
            setValueToVector(m.port, m.message);
            execute[0, 2] = true;

            resp = p4Chat.ReceiveString(client, remoteIp);
            m = JsonConvert
                .DeserializeObject<Models.Message>(resp);
            setVectorToVector(m.port, JsonConvert.DeserializeObject<string[]>(m.message));
            execute[1, 2] = true;
        }

        private void General_Load(object sender, EventArgs e)
        {
        }

        private void loadThreads()
        {
            t1 = new Thread(firstReceiver);
            t2 = new Thread(secondReceiver);
            t3 = new Thread(trirdReceiver);
            validator = new Thread(validate);

            t1.IsBackground = true;
            t2.IsBackground = true;
            t3.IsBackground = true;
            validator.IsBackground = true;

            t1.Start();
            t2.Start();
            t3.Start();
            validator.Start();
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            if (!checkFileds())
                return;

            connectButton.Enabled = false;

            ports = new int[] { p1, p2, p3, p4 };
            Array.Sort(ports);
            p1Label.Text = ports[0].ToString();
            p2Label.Text = ports[1].ToString();
            p3Label.Text = ports[2].ToString();
            p4Label.Text = ports[3].ToString();

            p1TextBox.Enabled = false;
            p2TextBox.Enabled = false;
            p3TextBox.Enabled = false;
            p4TextBox.Enabled = false;

            setConnect();
            loadThreads();
            sendButton.Enabled = true;
        }
    }
}
