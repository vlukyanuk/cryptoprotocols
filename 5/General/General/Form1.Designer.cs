﻿namespace General
{
    partial class General
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.sendButton = new System.Windows.Forms.Button();
            this.p2Message = new System.Windows.Forms.TextBox();
            this.p3Message = new System.Windows.Forms.TextBox();
            this.p4Message = new System.Windows.Forms.TextBox();
            this.p1TextBox = new System.Windows.Forms.TextBox();
            this.p2TextBox = new System.Windows.Forms.TextBox();
            this.p3TextBox = new System.Windows.Forms.TextBox();
            this.p4TextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.p1Answer = new System.Windows.Forms.TextBox();
            this.p2Answer = new System.Windows.Forms.TextBox();
            this.p3Answer = new System.Windows.Forms.TextBox();
            this.p4Answer = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.p1Message = new System.Windows.Forms.TextBox();
            this.connectButton = new System.Windows.Forms.Button();
            this.p1Label = new System.Windows.Forms.Label();
            this.p2Label = new System.Windows.Forms.Label();
            this.p3Label = new System.Windows.Forms.Label();
            this.p4Label = new System.Windows.Forms.Label();
            this.enemyCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // sendButton
            // 
            this.sendButton.BackColor = System.Drawing.Color.Red;
            this.sendButton.Enabled = false;
            this.sendButton.Location = new System.Drawing.Point(220, 174);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(75, 23);
            this.sendButton.TabIndex = 0;
            this.sendButton.Text = "Send";
            this.sendButton.UseVisualStyleBackColor = false;
            this.sendButton.Click += new System.EventHandler(this.sendButton_Click);
            // 
            // p2Message
            // 
            this.p2Message.Location = new System.Drawing.Point(12, 38);
            this.p2Message.Name = "p2Message";
            this.p2Message.Size = new System.Drawing.Size(100, 20);
            this.p2Message.TabIndex = 2;
            // 
            // p3Message
            // 
            this.p3Message.Location = new System.Drawing.Point(12, 64);
            this.p3Message.Name = "p3Message";
            this.p3Message.Size = new System.Drawing.Size(100, 20);
            this.p3Message.TabIndex = 3;
            // 
            // p4Message
            // 
            this.p4Message.Location = new System.Drawing.Point(12, 90);
            this.p4Message.Name = "p4Message";
            this.p4Message.Size = new System.Drawing.Size(100, 20);
            this.p4Message.TabIndex = 4;
            // 
            // p1TextBox
            // 
            this.p1TextBox.Location = new System.Drawing.Point(232, 12);
            this.p1TextBox.Name = "p1TextBox";
            this.p1TextBox.Size = new System.Drawing.Size(63, 20);
            this.p1TextBox.TabIndex = 5;
            // 
            // p2TextBox
            // 
            this.p2TextBox.Location = new System.Drawing.Point(232, 38);
            this.p2TextBox.Name = "p2TextBox";
            this.p2TextBox.Size = new System.Drawing.Size(63, 20);
            this.p2TextBox.TabIndex = 6;
            // 
            // p3TextBox
            // 
            this.p3TextBox.Location = new System.Drawing.Point(232, 64);
            this.p3TextBox.Name = "p3TextBox";
            this.p3TextBox.Size = new System.Drawing.Size(63, 20);
            this.p3TextBox.TabIndex = 7;
            // 
            // p4TextBox
            // 
            this.p4TextBox.Location = new System.Drawing.Point(232, 90);
            this.p4TextBox.Name = "p4TextBox";
            this.p4TextBox.Size = new System.Drawing.Size(63, 20);
            this.p4TextBox.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Location = new System.Drawing.Point(118, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Real                    Port";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Enabled = false;
            this.label2.Location = new System.Drawing.Point(118, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Message             Port";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Enabled = false;
            this.label3.Location = new System.Drawing.Point(118, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Message             Port";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Enabled = false;
            this.label4.Location = new System.Drawing.Point(118, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Message             Port";
            // 
            // p1Answer
            // 
            this.p1Answer.Enabled = false;
            this.p1Answer.Location = new System.Drawing.Point(12, 148);
            this.p1Answer.Name = "p1Answer";
            this.p1Answer.Size = new System.Drawing.Size(48, 20);
            this.p1Answer.TabIndex = 13;
            // 
            // p2Answer
            // 
            this.p2Answer.Enabled = false;
            this.p2Answer.Location = new System.Drawing.Point(67, 148);
            this.p2Answer.Name = "p2Answer";
            this.p2Answer.Size = new System.Drawing.Size(48, 20);
            this.p2Answer.TabIndex = 14;
            // 
            // p3Answer
            // 
            this.p3Answer.Enabled = false;
            this.p3Answer.Location = new System.Drawing.Point(121, 148);
            this.p3Answer.Name = "p3Answer";
            this.p3Answer.Size = new System.Drawing.Size(48, 20);
            this.p3Answer.TabIndex = 15;
            // 
            // p4Answer
            // 
            this.p4Answer.Enabled = false;
            this.p4Answer.Location = new System.Drawing.Point(175, 148);
            this.p4Answer.Name = "p4Answer";
            this.p4Answer.Size = new System.Drawing.Size(48, 20);
            this.p4Answer.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Enabled = false;
            this.label5.Location = new System.Drawing.Point(241, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Answer";
            // 
            // p1Message
            // 
            this.p1Message.Location = new System.Drawing.Point(12, 12);
            this.p1Message.Name = "p1Message";
            this.p1Message.Size = new System.Drawing.Size(100, 20);
            this.p1Message.TabIndex = 18;
            // 
            // connectButton
            // 
            this.connectButton.BackColor = System.Drawing.Color.Red;
            this.connectButton.Location = new System.Drawing.Point(12, 174);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75, 23);
            this.connectButton.TabIndex = 19;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = false;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // p1Label
            // 
            this.p1Label.AutoSize = true;
            this.p1Label.Location = new System.Drawing.Point(18, 132);
            this.p1Label.Name = "p1Label";
            this.p1Label.Size = new System.Drawing.Size(19, 13);
            this.p1Label.TabIndex = 20;
            this.p1Label.Text = "p1";
            // 
            // p2Label
            // 
            this.p2Label.AutoSize = true;
            this.p2Label.Location = new System.Drawing.Point(74, 132);
            this.p2Label.Name = "p2Label";
            this.p2Label.Size = new System.Drawing.Size(19, 13);
            this.p2Label.TabIndex = 21;
            this.p2Label.Text = "p2";
            // 
            // p3Label
            // 
            this.p3Label.AutoSize = true;
            this.p3Label.Location = new System.Drawing.Point(128, 132);
            this.p3Label.Name = "p3Label";
            this.p3Label.Size = new System.Drawing.Size(19, 13);
            this.p3Label.TabIndex = 22;
            this.p3Label.Text = "p3";
            // 
            // p4Label
            // 
            this.p4Label.AutoSize = true;
            this.p4Label.Location = new System.Drawing.Point(182, 132);
            this.p4Label.Name = "p4Label";
            this.p4Label.Size = new System.Drawing.Size(19, 13);
            this.p4Label.TabIndex = 23;
            this.p4Label.Text = "p4";
            // 
            // enemyCheckBox
            // 
            this.enemyCheckBox.AutoSize = true;
            this.enemyCheckBox.Location = new System.Drawing.Point(233, 122);
            this.enemyCheckBox.Name = "enemyCheckBox";
            this.enemyCheckBox.Size = new System.Drawing.Size(58, 17);
            this.enemyCheckBox.TabIndex = 24;
            this.enemyCheckBox.Text = "Enemy";
            this.enemyCheckBox.UseVisualStyleBackColor = true;
            // 
            // General
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.ClientSize = new System.Drawing.Size(301, 206);
            this.Controls.Add(this.enemyCheckBox);
            this.Controls.Add(this.p4Label);
            this.Controls.Add(this.p3Label);
            this.Controls.Add(this.p2Label);
            this.Controls.Add(this.p1Label);
            this.Controls.Add(this.connectButton);
            this.Controls.Add(this.p1Message);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.p4Answer);
            this.Controls.Add(this.p3Answer);
            this.Controls.Add(this.p2Answer);
            this.Controls.Add(this.p1Answer);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.p4TextBox);
            this.Controls.Add(this.p3TextBox);
            this.Controls.Add(this.p2TextBox);
            this.Controls.Add(this.p1TextBox);
            this.Controls.Add(this.p4Message);
            this.Controls.Add(this.p3Message);
            this.Controls.Add(this.p2Message);
            this.Controls.Add(this.sendButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "General";
            this.Text = "General";
            this.Load += new System.EventHandler(this.General_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.TextBox p2Message;
        private System.Windows.Forms.TextBox p3Message;
        private System.Windows.Forms.TextBox p4Message;
        private System.Windows.Forms.TextBox p1TextBox;
        private System.Windows.Forms.TextBox p2TextBox;
        private System.Windows.Forms.TextBox p3TextBox;
        private System.Windows.Forms.TextBox p4TextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox p1Answer;
        private System.Windows.Forms.TextBox p2Answer;
        private System.Windows.Forms.TextBox p3Answer;
        private System.Windows.Forms.TextBox p4Answer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox p1Message;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.Label p1Label;
        private System.Windows.Forms.Label p2Label;
        private System.Windows.Forms.Label p3Label;
        private System.Windows.Forms.Label p4Label;
        private System.Windows.Forms.CheckBox enemyCheckBox;
    }
}

