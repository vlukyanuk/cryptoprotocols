﻿using System;
using System.Numerics;
using EEAlib;

namespace ExtendsEvklidAlgoritm
{
    class Program
    {
        static void Main(string[] args)
        {
            BigInteger a, b, x, y, d;
            Console.WriteLine("ExtendsEvklidAlgoritm\nEnter a: ");
            a = Int32.Parse(Console.ReadLine());
             Console.WriteLine("Enter b: ");
            b = Int32.Parse(Console.ReadLine());
            d = EEAlib.lib.gcd(a, b, out x, out y);
            Console.WriteLine($"Answer is x = {x} y = {y} d = {d}");
        }
    }
}
