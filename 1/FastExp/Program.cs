﻿using System;
using FastExpLib;

namespace FastExp
{
    class Program
    {
        static void Main(string[] args)
        {
            int a,m,p;
            Console.WriteLine("An example a^m(p)\nEnter a: ");
            a = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Enter m: ");
            m = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Enter p: ");
            p = Int32.Parse(Console.ReadLine());
            Console.WriteLine($"Answer is {FastExpLib.lib.calculation(a,m,p)}");
        }
    }
}
