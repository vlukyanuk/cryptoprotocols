using System;
using System.Collections;
using System.Numerics;

namespace FastExpLib
{
    public class lib
    {
        private static BitArray getBitsFromM(BigInteger m)
        {
            return new BitArray(m.ToByteArray());
        }

        private static int getMCountDigit(BigInteger m)
        {
            return (int) Math.Truncate(BigInteger.Log(m, 2)) + 1;
        }

        private static BigInteger[] getArray(BigInteger a, BigInteger p, int count)
        {
            BigInteger[] array = new BigInteger[count];
            for(int index = 0; index < count; index++)
                array[index] = index == 0 ? a : (array[index - 1] * array[index - 1]) % p;
            return array;
        }

        private static BigInteger mul(BigInteger[] array, BitArray bitArray)
        {
            BigInteger buffer = 1;
            for(int index = 0; index < array.Length; index++)
                buffer *= bitArray.Get(index) ? array[index] : 1;
            return buffer;
        }

        private static BigInteger answer(BigInteger preCalc, BigInteger p)
        {
            return preCalc % p;
        }

        public static BigInteger calculation(BigInteger a, BigInteger m, BigInteger p)
        {
            BitArray bits = getBitsFromM(m);
            int n = getMCountDigit(m);
            BigInteger[] array = getArray(a, p, n);
            BigInteger preCalc = mul(array, bits);            
            return answer(preCalc, p);
        }
    }
}