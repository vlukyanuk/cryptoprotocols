﻿using System;
using Xunit;
using FastExpLib;
using HODlib;
using EEAlib;
using SimpleNumberLib;
using System.Numerics;

namespace test
{
    public class Tests
    {
        [Fact]
        public void TestFastExponential()
        {
            string a = "21474836473456789278361072";
            string m = "2147483647345678";
            string p = "214748364722312";
            BigInteger bigA, bigM, bigP;
            BigInteger.TryParse (a, out bigA);
            BigInteger.TryParse (m, out bigM);
            BigInteger.TryParse (p, out bigP);
            BigInteger answer = BigInteger.ModPow(bigA, bigM, bigP);
            Assert.Equal(answer, FastExpLib.lib.calculation(bigA,bigM,bigP));
        }

        [Fact]
        public void TestHOD()
        {
            string a = "1234567890987654321123456789";
            string b = "5126593716529365129756";
            BigInteger bigA, bigB;
            BigInteger.TryParse(a, out bigA);
            BigInteger.TryParse(b, out bigB);
            BigInteger answer = BigInteger.GreatestCommonDivisor(bigA, bigB);
            Assert.Equal(answer, HODlib.lib.calculate( bigA, bigB));
        }

        [Fact]
        public void TestExtendsEvklidAlgoritm()
        {
            string a = "1234567890987654321123456789";
            string b = "5126593716529365129756";
            BigInteger bigA, bigB, xAnswer, yAnswer;
            BigInteger.TryParse(a, out bigA);
            BigInteger.TryParse(b, out bigB);
            
            BigInteger x,y;
            BigInteger dAnswer = BigInteger.GreatestCommonDivisor(bigA, bigB);
            BigInteger.TryParse("478593333684553004459", out xAnswer);
            BigInteger.TryParse("-115253128154592837105212983", out yAnswer);

            Assert.Equal(dAnswer, EEAlib.lib.gcd(bigA, bigB, out x, out y));
            Assert.Equal(xAnswer, x);
            Assert.Equal(yAnswer, y);
        }

        [Fact]
        public void TestSimpleNumberLin()
        {
            int bites = 128;
            BigInteger number = SimpleNumberLib.lib.generate(bites);
            Assert.True(SimpleNumberLib.lib.MillerRabinTest(number));
            BigInteger ownNumber, ownNumber2;
            BigInteger.TryParse("106266205661526535851980616468565657961", out ownNumber);
            Assert.True(SimpleNumberLib.lib.MillerRabinTest(ownNumber));
            BigInteger.TryParse("489133282872437279", out ownNumber2);
            Assert.True(SimpleNumberLib.lib.MillerRabinTest(ownNumber2));
        }
    }
}
