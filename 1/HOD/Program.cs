﻿using System;
using HODlib;

namespace HOD
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;
            Console.WriteLine("HOD a and b\nEnter a: ");
            a = Int32.Parse(Console.ReadLine());
             Console.WriteLine("Enter b: ");
            b = Int32.Parse(Console.ReadLine());
            Console.WriteLine($"Answer is {HODlib.lib.calculate(a,b)}");
        }
    }
}
