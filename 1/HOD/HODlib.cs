using System;
using System.Numerics;

namespace HODlib
{
    public class lib
    {
        private static BigInteger rest(BigInteger value, BigInteger mod)
        {
            return value % mod;
        }

        public static BigInteger calculate(BigInteger a, BigInteger b)
        {
            while((a != 0) && (b != 0))
            {
                if(a > b)
                {
                    a = rest(a , b);
                    continue;
                }
                b = rest(b , a);
            }
            return a + b;
        }
    }
}