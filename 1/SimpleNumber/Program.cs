﻿using System;
using SimpleNumberLib;

namespace SimpleNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            int cout;
            Console.WriteLine("Generate simple number and test it\nEnter bits count: ");
            cout = Int32.Parse(Console.ReadLine());
            Console.WriteLine($"Answer is {SimpleNumberLib.lib.generate(cout)}");
        }
    }
}
