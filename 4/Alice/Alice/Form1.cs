﻿using System;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Security.Cryptography;
using Newtonsoft.Json;
using Alice.Models;

namespace Alice
{
    public partial class Alice : Form
    {
        private Chat chat;
        private Chat server;
        private string rmAddress;
        private int rmPort;
        private int lcPort;
        private int srPort;
        private byte[] sessionkey;
        private Thread receiveThread;
        UdpClient client;
        IPEndPoint remoteIp;

        public Alice()
        {
            InitializeComponent();
            rmPort = 8081;
            lcPort = 8080;
            srPort = 8082;
            rmAddress = "127.0.0.1";
            client = new UdpClient(lcPort);
            remoteIp = null;
        }

        private void Alice_Load(object sender, EventArgs e)
        {
            setConnect();
        }

        private void startProtocol()
        {
            Random rnd = new Random();
            int rndNumber = rnd.Next(0, int.MaxValue);
            RSAParameters privateKey = RSA.GetPrivateKey();
            RSAParameters publicKey = RSA.GetPublicKey();
            RSAParameters bobPublicKey = new RSAParameters();
            RSAParameters trentPublicKey = new RSAParameters();

            byte[] decryptM = null;
            byte[] encryptM = null;
            InitMessage m = null;
            signedMessage sm = null;

            string json = JsonConvert.SerializeObject
                (
                    publicKey
                );

            server.SendMessage(json);

            json = server.ReceiveString(client, remoteIp);

            trentPublicKey = JsonConvert.DeserializeObject
                <RSAParameters>(json);
            ////////////////////////////////////////////////

            json = JsonConvert.SerializeObject
                (
                    new InitMessage()
                    {
                        payload1 = Encoding.Default.GetBytes("Alice"),
                        payload2 = Encoding.Default.GetBytes("Bob")
                    }
                );

            server.SendMessage(json);
            ////////////////////////////////////////////////

            json = server.ReceiveString(client, remoteIp);

            sm = JsonConvert.DeserializeObject<signedMessage>(json);
            if(!RSA.VerifyMessage(sm.message, sm.sign, trentPublicKey))
                MessageBox.Show("Malary is here!!!");
            json = Encoding.Default.GetString(sm.message);
            m = JsonConvert.DeserializeObject<InitMessage>(json);
            string bobO = Encoding.Default.GetString(m.payload2);
            bobPublicKey = JsonConvert.DeserializeObject<RSAParameters>(bobO);
            ////////////////////////////////////////////////
            byte[] rndBytes = BitConverter.GetBytes(rndNumber);
            encryptM = RSA.RSAEncrypt
                (
                    rndBytes,
                    bobPublicKey,
                    false
                );
            json = JsonConvert.SerializeObject
                (
                    new InitMessage()
                    {
                        payload1 = Encoding.Default.GetBytes("Alice"),
                        payload2 = encryptM
                    }
                );
            chat.SendMessage(json);
            ////////////////////////////////////////////////
            json = chat.ReceiveString(client, remoteIp);
            decryptM = RSA.RSADecryptM
                        (
                        Encoding.Default.GetBytes(json),
                        privateKey,
                        false
                        );
            json = Encoding.Default.GetString(decryptM);
            m = JsonConvert.DeserializeObject<InitMessage>(json);
            sm = JsonConvert.DeserializeObject<signedMessage>
                (
                    Encoding.Default.GetString(m.payload1)
                );
            if (!RSA.VerifyMessage(sm.message, sm.sign, trentPublicKey))
                MessageBox.Show("Malary is here!!!");
            json = Encoding.Default.GetString(sm.message);
            ExtendedMessage em = JsonConvert.DeserializeObject<ExtendedMessage>(json);
            int newRNDNumber = BitConverter.ToInt32(em.payload1, 0);

            if(rndNumber != newRNDNumber)
                MessageBox.Show("Malary is here!!!");

            sessionkey = em.payload2;

            ////////////////////////////////////////////////
            json = EncryptDecryptMessage
                (
                    Encoding.Default.GetString(m.payload2),
                    sessionkey
                );

            chat.SendMessage(json);

            sendButton.Enabled = true;
            sendMessageTextBox.Enabled = true;
            receiveThread.Start();
        }

        private void setConnect()
        {
            chat = new Chat(
                rmAddress,
                rmPort,
                lcPort,
                client
                );

            server = new Chat(
                rmAddress,
                srPort,
                lcPort,
                client
                );

            receiveThread = new Thread
                (
                    new ThreadStart(ReceiveMessage)
                );
            receiveThread.IsBackground = true;

            PrintMessage("Connection is active...protocol waits...");
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            chat.SendMessage(EncryptDecryptMessage(sendMessageTextBox.Text, sessionkey));
            PrintMessage(sendMessageTextBox.Text);
        }

        private void PrintMessage(string message)
        {
            messagesTextBox.Invoke
                (
                    new Action
                    (
                        () => messagesTextBox.Text = String.Concat
                        (
                            messagesTextBox.Text,
                            Environment.NewLine,
                            message
                        )
                    )
                );
        }

        private string EncryptDecryptMessage(string message, byte[] key)
        {
            byte[] sessionBytes = key;
            byte[] messageBytes = Encoding.Default.GetBytes(message);
            for (int i = 0; i < messageBytes.Length; i++)
            {
                messageBytes[i] = (byte)(messageBytes[i] ^ sessionBytes[i % sessionBytes.Length]);
            }
            return Encoding.Default.GetString(messageBytes);
        }

        private void ReceiveMessage()
        {
            try
            {
                while (true)
                {
                    string message = EncryptDecryptMessage(chat.ReceiveString(client, remoteIp), sessionkey);
                    PrintMessage(message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed connection.");
            }
        }

        private void startProtocolButton_Click(object sender, EventArgs e)
        {
            startProtocol();
        }
    }
}
