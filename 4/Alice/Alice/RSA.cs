﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Alice
{
    class RSA
    {
        private static RSACryptoServiceProvider RSACSP = new RSACryptoServiceProvider();

        public static RSAParameters GetPublicKey()
        {
            return RSACSP.ExportParameters(false);
        }

        public static RSAParameters GetPrivateKey()
        {
            return RSACSP.ExportParameters(true);
        }

        public static byte[] SignMessage(byte[] message, RSAParameters privateKey)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.ImportParameters(privateKey);
            return rsa.SignData
            (
                message,
                CryptoConfig.MapNameToOID("SHA256")
            );
        }

        public static bool VerifyMessage(byte[] message,byte[] sign, RSAParameters publicKey)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.ImportParameters(publicKey);
            return rsa.VerifyData(message, CryptoConfig.MapNameToOID("SHA256"), sign);
        }

        public static byte[] RSAEncrypt(byte[] DataToEncrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        {
            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
            {
                RSA.ImportParameters(RSAKeyInfo);
                return RSA.Encrypt(DataToEncrypt, DoOAEPPadding);
            }
        }

        public static byte[] RSADecrypt(byte[] DataToDecrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        {
            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
            {
                RSA.ImportParameters(RSAKeyInfo);
                return RSA.Decrypt(DataToDecrypt, DoOAEPPadding);
            }
        }

        public static byte[] RSAEncryptM(byte[] DataToEncrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        {
            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
            {
                RSA.ImportParameters(RSAKeyInfo);
                return DataToEncrypt;
            }
        }

        public static byte[] RSADecryptM(byte[] DataToDecrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        {
            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
            {
                RSA.ImportParameters(RSAKeyInfo);
                return DataToDecrypt;
            }
        }
    }
}
