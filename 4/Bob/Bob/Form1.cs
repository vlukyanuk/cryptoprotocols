﻿using System;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Security.Cryptography;
using Newtonsoft.Json;
using Bob.Models;

namespace Bob
{
    public partial class Bob : Form
    {
        private Chat chat;
        private Chat server;
        private string rmAddress;
        private int rmPort;
        private int lcPort;
        private int srPort;
        private byte[] sessionkey;
        private Thread receiveThread;
        UdpClient client;
        IPEndPoint remoteIp;

        public Bob()
        {
            InitializeComponent();
            rmPort = 8080;
            lcPort = 8081;
            srPort = 8082;
            rmAddress = "127.0.0.1";
            client = new UdpClient(lcPort);
            remoteIp = null;
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            chat.SendMessage(EncryptDecryptMessage(sendMessageTextBox.Text, sessionkey));
            PrintMessage(sendMessageTextBox.Text);
        }

        private void Bob_Load(object sender, EventArgs e)
        {
            setConnect();
        }

        private void startProtocol()
        {
            Random rnd = new Random();
            int rndNumber = rnd.Next(0, int.MaxValue);
            RSAParameters privateKey = RSA.GetPrivateKey();
            RSAParameters publicKey = RSA.GetPublicKey();
            RSAParameters alicePublicKey = new RSAParameters();
            RSAParameters trentPublicKey = new RSAParameters();

            byte[] decryptM = null;
            byte[] encryptM = null;
            InitMessage m = null;
            signedMessage sm = null;

            string json = JsonConvert.SerializeObject
                (
                    publicKey
                );

            server.SendMessage(json);

            json = server.ReceiveString(client, remoteIp);

            trentPublicKey = JsonConvert.DeserializeObject
                <RSAParameters>(json);
            ////////////////////////////////////////////////

            json = chat.ReceiveString(client, remoteIp);
            m = JsonConvert.DeserializeObject<InitMessage>(json);
            decryptM = RSA.RSADecrypt
                (
                    m.payload2,
                    privateKey,
                    false
                );
            ////////////////////////////////////////////////

            encryptM = RSA.RSAEncrypt
                (
                    decryptM,
                    trentPublicKey,
                    false
                );
            json = JsonConvert.SerializeObject
                (
                    new ttob()
                    {
                        payload1 = Encoding.Default.GetBytes("Alice"),
                        payload2 = Encoding.Default.GetBytes("Bob"),
                        payload3 = encryptM
                    }
                );
            server.SendMessage(json);
            ////////////////////////////////////////////////

            json = server.ReceiveString(client, remoteIp);
            m = JsonConvert.DeserializeObject<InitMessage>(json);

            sm = JsonConvert.DeserializeObject<signedMessage>
                (
                    Encoding.Default.GetString(m.payload1)
                );
            if (!RSA.VerifyMessage(sm.message, sm.sign, trentPublicKey))
                MessageBox.Show("Malary is here!!!");
            json = Encoding.Default.GetString(sm.message);
            alicePublicKey = JsonConvert.DeserializeObject<RSAParameters>(json);

            decryptM = RSA.RSADecryptM
                (
                    m.payload2,
                    privateKey,
                    false
                );

            sm = JsonConvert.DeserializeObject<signedMessage>
                (
                    Encoding.Default.GetString(decryptM)
                );
            if (!RSA.VerifyMessage(sm.message, sm.sign, trentPublicKey))
                MessageBox.Show("Malary is here!!!");
            json = Encoding.Default.GetString(sm.message);
            ExtendedMessage em = JsonConvert.DeserializeObject<ExtendedMessage>(json);
            sessionkey = em.payload2;
            ////////////////////////////////////////////////

            m = new InitMessage()
            {
                payload1 = decryptM,
                payload2 = BitConverter.GetBytes(rndNumber)
            };
            json = JsonConvert.SerializeObject(m);
            encryptM = RSA.RSAEncryptM
                (
                    Encoding.Default.GetBytes(json),
                    alicePublicKey,
                    false
                );
            json = Encoding.Default.GetString(encryptM);
            chat.SendMessage(json);
            ////////////////////////////////////////////////

            json = chat.ReceiveString(client, remoteIp);
            json = EncryptDecryptMessage(json, sessionkey);
            int newRndNumber = BitConverter.ToInt32
                (
                    Encoding.Default.GetBytes(json),
                    0
                );

            if (newRndNumber != rndNumber)
                MessageBox.Show("Malary is here!!!");

            sendButton.Enabled = true;
            sendMessageTextBox.Enabled = true;
            receiveThread.Start();
        }

        private void setConnect()
        {
            chat = new Chat(
                rmAddress,
                rmPort,
                lcPort,
                client
                );

            server = new Chat(
                rmAddress,
                srPort,
                lcPort,
                client
                );

            receiveThread = new Thread
                (
                    new ThreadStart(ReceiveMessage)
                );
            receiveThread.IsBackground = true;

            PrintMessage("Connection is active...protocol waits...");
        }

        private void PrintMessage(string message)
        {
            messagesTextBox.Invoke
                (
                    new Action
                    (
                        () => messagesTextBox.Text = String.Concat
                        (
                            messagesTextBox.Text,
                            Environment.NewLine,
                            message
                        )
                    )
                );
        }

        private string EncryptDecryptMessage(string message, byte[] key)
        {
            byte[] sessionBytes = key;
            byte[] messageBytes = Encoding.Default.GetBytes(message);
            for (int i = 0; i < messageBytes.Length; i++)
            {
                messageBytes[i] = (byte)(messageBytes[i] ^ sessionBytes[i % sessionBytes.Length]);
            }
            return Encoding.Default.GetString(messageBytes);
        }

        private void ReceiveMessage()
        {
            try
            {
                while (true)
                {
                    string message = EncryptDecryptMessage(chat.ReceiveString(client, remoteIp), sessionkey);
                    PrintMessage(message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed connection.");
            }
        }

        private void startProtocolButton_Click(object sender, EventArgs e)
        {
            startProtocol();
        }
    }
}
