﻿namespace Bob
{
    partial class Bob
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.sendMessageTextBox = new System.Windows.Forms.TextBox();
            this.sendButton = new System.Windows.Forms.Button();
            this.messagesTextBox = new System.Windows.Forms.TextBox();
            this.startProtocolButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // sendMessageTextBox
            // 
            this.sendMessageTextBox.Enabled = false;
            this.sendMessageTextBox.Location = new System.Drawing.Point(12, 186);
            this.sendMessageTextBox.Name = "sendMessageTextBox";
            this.sendMessageTextBox.Size = new System.Drawing.Size(248, 20);
            this.sendMessageTextBox.TabIndex = 21;
            // 
            // sendButton
            // 
            this.sendButton.Enabled = false;
            this.sendButton.Location = new System.Drawing.Point(266, 184);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(75, 23);
            this.sendButton.TabIndex = 20;
            this.sendButton.Text = "Send";
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.sendButton_Click);
            // 
            // messagesTextBox
            // 
            this.messagesTextBox.Location = new System.Drawing.Point(12, 12);
            this.messagesTextBox.Multiline = true;
            this.messagesTextBox.Name = "messagesTextBox";
            this.messagesTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.messagesTextBox.Size = new System.Drawing.Size(438, 166);
            this.messagesTextBox.TabIndex = 19;
            // 
            // startProtocolButton
            // 
            this.startProtocolButton.Location = new System.Drawing.Point(347, 184);
            this.startProtocolButton.Name = "startProtocolButton";
            this.startProtocolButton.Size = new System.Drawing.Size(103, 23);
            this.startProtocolButton.TabIndex = 22;
            this.startProtocolButton.Text = "Start Protocol";
            this.startProtocolButton.UseVisualStyleBackColor = true;
            this.startProtocolButton.Click += new System.EventHandler(this.startProtocolButton_Click);
            // 
            // Bob
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(465, 216);
            this.Controls.Add(this.startProtocolButton);
            this.Controls.Add(this.sendMessageTextBox);
            this.Controls.Add(this.sendButton);
            this.Controls.Add(this.messagesTextBox);
            this.Name = "Bob";
            this.Text = "Bob";
            this.Load += new System.EventHandler(this.Bob_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox sendMessageTextBox;
        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.TextBox messagesTextBox;
        private System.Windows.Forms.Button startProtocolButton;
    }
}

