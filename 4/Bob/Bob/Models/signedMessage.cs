﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bob.Models
{
    class signedMessage
    {
        public byte[] sign { get; set; }
        public byte[] message { get; set; }
    }
}
