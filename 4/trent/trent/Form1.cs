﻿using System;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Security.Cryptography;
using Newtonsoft.Json;
using trent.Models;
using System.Linq;

namespace trent
{
    public partial class Trent : Form
    {
        private Chat alice;
        private Chat bob;
        private string rmAddress;
        private int alPort;
        private int lcPort;
        private int boPort;
        private byte[] sessionkey;
        private Thread receiveThread;
        UdpClient client;
        IPEndPoint remoteIp;

        public Trent()
        {
            InitializeComponent();
            alPort = 8080;
            lcPort = 8082;
            boPort = 8081;
            rmAddress = "127.0.0.1";
            client = new UdpClient(lcPort);
            remoteIp = null;
        }

        private void setConnect()
        {
            alice = new Chat(
                rmAddress,
                alPort,
                lcPort,
                client
                );

            bob = new Chat(
                rmAddress,
                boPort,
                lcPort,
                client
                );
        }

        private string RandomString(int length)
        {
            Random random = new Random();
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private void startProtocol()
        {
            RSAParameters privateKey = RSA.GetPrivateKey();
            RSAParameters publicKey = RSA.GetPublicKey();
            RSAParameters alicePublicKey = new RSAParameters();
            RSAParameters bobPublicKey = new RSAParameters();

            byte[] decryptM = null;
            byte[] encryptM = null;
            InitMessage m = null;
            signedMessage sm = null;
            sessionkey
                = Encoding.Default.GetBytes(RandomString(32));

            string json = alice.ReceiveString(client, remoteIp);
            alicePublicKey = JsonConvert.DeserializeObject<RSAParameters>(json);
            json = JsonConvert.SerializeObject
                (
                    publicKey
                );

            alice.SendMessage(json);

            json = bob.ReceiveString(client, remoteIp);
            bobPublicKey = JsonConvert.DeserializeObject<RSAParameters>(json);
            json = JsonConvert.SerializeObject
                (
                    publicKey
                );

            bob.SendMessage(json);
            ////////////////////////////////////////////////
            
            json = alice.ReceiveString(client, remoteIp);
            m = JsonConvert.DeserializeObject<InitMessage>(json);
            ////////////////////////////////////////////////

            json = JsonConvert.SerializeObject(bobPublicKey);
            m = new InitMessage()
            {
                payload1 = m.payload2,
                payload2 = Encoding.Default.GetBytes(json)
            };
            json = JsonConvert.SerializeObject(m);
            sm = new signedMessage()
            {
                sign = RSA.SignMessage(Encoding.Default.GetBytes(json), privateKey),
                message = Encoding.Default.GetBytes(json)
            };
            json = JsonConvert.SerializeObject(sm);
            alice.SendMessage(json);
            ////////////////////////////////////////////////
            
            json = bob.ReceiveString(client, remoteIp);
            ttob t = JsonConvert.DeserializeObject<ttob>(json);
            decryptM = RSA.RSADecrypt
                (
                    t.payload3,
                    privateKey,
                    false
                );
            ////////////////////////////////////////////////

            ExtendedMessage em = new ExtendedMessage()
            {
                payload1 = decryptM,
                payload2 = sessionkey,
                payload3 = t.payload1,
                payload4 = t.payload2
            };
            json = JsonConvert.SerializeObject(em);
            sm = new signedMessage()
            {
                sign = RSA.SignMessage(Encoding.Default.GetBytes(json), privateKey),
                message = Encoding.Default.GetBytes(json)
            };
            json = JsonConvert.SerializeObject(sm);
            encryptM = RSA.RSAEncryptM
                (
                    Encoding.Default.GetBytes(json),
                    bobPublicKey,
                    false
                );

            json = JsonConvert.SerializeObject(alicePublicKey);
            sm = new signedMessage()
            {
                sign = RSA.SignMessage(Encoding.Default.GetBytes(json), privateKey),
                message = Encoding.Default.GetBytes(json)
            };
            json = JsonConvert.SerializeObject(sm);

            m = new InitMessage()
            {
                payload1 = Encoding.Default.GetBytes(json),
                payload2 = encryptM
            };

            json = JsonConvert.SerializeObject(m);
            bob.SendMessage(json);

            Application.Exit();
        }

        private void Trent_Load(object sender, EventArgs e)
        {
            setConnect();
        }

        private void startProtocolButton_Click(object sender, EventArgs e)
        {
            startProtocol();
        }
    }
}
