﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trent.Models
{
    class ExtendedMessage
    {
        public byte[] payload1 { get; set; }
        public byte[] payload2 { get; set; }
        public byte[] payload3 { get; set; }
        public byte[] payload4 { get; set; }
    }
}
