﻿namespace trent
{
    partial class Trent
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Server = new System.Windows.Forms.Label();
            this.startProtocolButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Server
            // 
            this.Server.AutoSize = true;
            this.Server.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Server.Location = new System.Drawing.Point(3, 2);
            this.Server.Name = "Server";
            this.Server.Size = new System.Drawing.Size(166, 55);
            this.Server.TabIndex = 0;
            this.Server.Text = "Server";
            // 
            // startProtocolButton
            // 
            this.startProtocolButton.Location = new System.Drawing.Point(34, 60);
            this.startProtocolButton.Name = "startProtocolButton";
            this.startProtocolButton.Size = new System.Drawing.Size(103, 23);
            this.startProtocolButton.TabIndex = 24;
            this.startProtocolButton.Text = "Start Protocol";
            this.startProtocolButton.UseVisualStyleBackColor = true;
            this.startProtocolButton.Click += new System.EventHandler(this.startProtocolButton_Click);
            // 
            // Trent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.ClientSize = new System.Drawing.Size(173, 90);
            this.Controls.Add(this.startProtocolButton);
            this.Controls.Add(this.Server);
            this.Name = "Trent";
            this.Text = "Trent";
            this.Load += new System.EventHandler(this.Trent_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Server;
        private System.Windows.Forms.Button startProtocolButton;
    }
}

